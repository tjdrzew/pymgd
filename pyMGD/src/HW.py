import pyMGDLib as MGD
import numpy as np

# Cross Section Data
MGD.comps.append(MGD.XS('A'))
MGD.comps[0].D    = np.array([1.3880   , 0.3842])
MGD.comps[0].Sa   = np.array([0.00971  , 0.1218])
MGD.comps[0].nSf  = np.array([0.006499 , 0.14290])
MGD.comps[0].kSf  = np.array([5.9494e-14 , 1.8314e-12])
MGD.comps[0].Sgg  = np.array([0.01422  , 0.0])
MGD.comps[0].adfW = np.array([0.9976   , 0.7996])
MGD.comps[0].adfE = np.array([1.0020   , 1.1390])
MGD.comps[0].adfS = np.array([0.9976   , 0.7996])
MGD.comps[0].adfN = np.array([1.0020   , 1.1390])

MGD.comps.append(MGD.XS('B'))
MGD.comps[1].D    = np.array([1.4470   , 0.3748])
MGD.comps[1].Sa   = np.array([0.00942   , 0.08929])
MGD.comps[1].nSf  = np.array([0.006505 , 0.13250])
MGD.comps[1].kSf  = np.array([5.9549e-14 , 1.6981e-12])
MGD.comps[1].Sgg  = np.array([0.01730  , 0.0])
MGD.comps[1].adfW = np.array([0.9939   , 1.1180])
MGD.comps[1].adfE = np.array([1.0040   , 0.9222])
MGD.comps[1].adfS = np.array([0.9939   , 1.1180])
MGD.comps[1].adfN = np.array([1.0040   , 0.9222])

MGD.comps.append(MGD.XS('W'))
MGD.comps[2].D    = np.array([1.5450   , 0.3126])
MGD.comps[2].Sa   = np.array([0.000444 , 0.008736])
MGD.comps[2].nSf  = np.array([0.0      , 0.0])
MGD.comps[2].kSf  = np.array([0.0      , 0.0])
MGD.comps[2].Sgg  = np.array([0.02838  , 0.0])
MGD.comps[2].adfW = np.array([1.0      , 1.0])
MGD.comps[2].adfE = np.array([1.0      , 1.0])
MGD.comps[2].adfS = np.array([1.0      , 1.0])
MGD.comps[2].adfN = np.array([1.0      , 1.0])

# Geometry
MGD.nxmesh    = 4
MGD.nymesh    = 4
MGD.xmesh     = np.array([4.0,8.0,8.0,8.0])
MGD.ymesh     = np.array([4.0,8.0,8.0,8.0])
MGD.nfxmesh   = np.array([1,2,2,2]) 
MGD.nfymesh   = np.array([1,2,2,2]) 

MGD.Rad_Conf = np.array([['A','B','A','W'],
                         ['B','A','B','W'],
                         ['A','B','A','W'],
                         ['W','W','W','W']])

# Boundary Conditions
MGD.AlbedoN = 1.0       # Reflective BC on North Face
MGD.AlbedoS = 0.031758  # Vacuum BC on South Face
MGD.AlbedoE = 0.031758  # Vacuum BC on East Face
MGD.AlbedoW = 1.0       # Reflective BC on West Face

# Solver Parameters
MGD.power = 1.0e-4
MGD.eps   = 1.0e-6
MGD.maxIt = 100

# Execute Code
MGD.pyMGD()
# MGD.testInterface(0)
# MGD.testInterface(1)



