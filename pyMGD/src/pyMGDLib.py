import numpy as np

# Global Arrays
comps     = []

# Course Mesh Arrays
Rad_Conf   = []
RadConfIdx = []
xmesh      = []
ymesh      = []
nfxmesh    = []
nfymesh    = []

# Node-wise parameters
nodes      = []
NIR        = []
phi1       = []
phi2       = []
powVec     = []
A          = []
Sfn        = []
Sfnp1      = []
Ss         = []

# Global Scalars
nxmesh   = 0
nymesh   = 0
nnodes   = 0
keff     = 1.0
AlbedoN  = 0.0 # Default to Vacuum BC on North Face
AlbedoS  = 0.0 # Default to Vacuum BC on South Face
AlbedoE  = 0.0 # Default to Vacuum BC on East Face
AlbedoW  = 0.0 # Default to Vacuum BC on West Face
power    = 1.0e-6  # MW
groups   = 2
xDim     = 0
yDim     = 0

eps      = 1.0e-6
maxIt    = 1

class XS(object):
	def __init__(self,name):
		self.name = name
		self.D    = []
		self.Sa   = []
		self.nSf  = []
		self.kSf  = []
		self.Sgg  = []
		self.adfN = []
		self.adfS = []
		self.adfE = []
		self.adfW = []

class node(object):
	def __init__(self):
		self.compIdx  = 0
		self.hx       = 0.0
		self.hy       = 0.0
		self.V        = 0.0
		self.NNidx    = -1    # north neighbor idx
		self.SNidx    = -1    # south neighbor idx
		self.ENidx    = -1    # east neighbor idx
		self.WNidx    = -1    # west neighbor idx		
		self.Gx       = []
		self.Gy       = []
		self.Sa       = []
		self.nSf      = []
		self.kSf      = []		
		self.Sgg      = []
		self.adfN     = []
		self.adfS     = []
		self.adfE     = []
		self.adfW     = []
	
def pyMGD():
	
	global phi1, phi2, powVec
	
	read = readRadConf()
	if read == 0:
		procInput()
		allocMem()
		init()
		solveEig()
		
		print '\n'
		print 'phi1'
		printVec(phi1)
		print '\n'
		print 'phi2'
		printVec(phi2) 	
		print '\n'
		print 'Power'
		printVec(powVec) 			
		
def readRadConf():
	global Rad_Conf, RadConfIdx, xmesh, ymesh, nfxmesh, nfymesh
	
	RadConfIdx = np.zeros([nymesh, nxmesh],dtype=np.int)
	for i in range(nymesh):
		for j in range(nxmesh):
			compName = Rad_Conf[i,j]
			if compName == 'X':
				RadConfIdx[i,j] = -1
			else:
				compIdx = getCompIdx(compName)
				if (compIdx == -1):
					print 'Unable to find composition "' + compName + '" located at (i,j) = (' + str(i) + ',' + str(j) + ')'
					return -1
				else:
					RadConfIdx[i,j] = compIdx
	return 0	
		
def procInput():
	
	global comps, Rad_Conf, RadConfIdx, xmesh, ymesh, nfxmesh, nfymesh
	global nodes, NIR
	global nxmesh, nymesh, nnodes, groups, xDim, yDim
	
	# create temporary matrices to store fine mesh data
	# fine mesh dimensions
	xDim = 0
	for i in range(nxmesh):
		xDim = xDim + nfxmesh[i]
	yDim = 0
	for i in range(nymesh):
		yDim = yDim + nfymesh[i]
		
	# store comp and  adf info in temporary fine mesh data structure
	fRadConfIdx = np.zeros([yDim,xDim],dtype=np.int) 
	adfN        = np.zeros([yDim,xDim],dtype=np.int) 
	adfS        = np.zeros([yDim,xDim],dtype=np.int) 
	adfE        = np.zeros([yDim,xDim],dtype=np.int) 
	adfW        = np.zeros([yDim,xDim],dtype=np.int) 		
		
	iii = 0
	for i in range (nymesh):
		for ii in range(nfymesh[i]):
			jjj = 0
			for j in range(nxmesh):
				compIdx = RadConfIdx[i,j]
				for jj in range(nfxmesh[j]):
					fRadConfIdx[iii,jjj] = compIdx
					if (ii == 0):
						adfN[iii,jjj] = 1
					if (ii == (nfymesh[i]-1)):
						adfS[iii,jjj] = 1
					if (jj == 0):
						adfW[iii,jjj] = 1
					if (jj == (nfxmesh[j]-1)):
						adfE[iii,jjj] =  1
					jjj = jjj + 1						
			iii = iii + 1

	hx  = np.zeros(xDim)
	k = 0
	for i in range(nxmesh):
		hxT = xmesh[i]/nfxmesh[i]
		for j in range(nfxmesh[i]):
			hx[k] = hxT
			k = k + 1
	hy  = np.zeros(yDim)
	k = 0
	for i in range(nymesh):
		hyT = ymesh[i]/nfymesh[i]
		for j in range(nfymesh[i]):
			hy[k] = hyT
			k = k + 1
				
	nodeIdxMat = np.zeros([yDim,xDim],dtype=np.int)
	NIR        = np.zeros(yDim,dtype=np.int)
	nnodes = 0
	for i in range(yDim):
		nodesInRow = 0
		for j in range(xDim):
			compIdx = fRadConfIdx[i,j]
			if (compIdx == -1):
				nodeIdxMat[i,j] = -1
			else:
				nnodes = nnodes + 1
				nodeIdx = nnodes - 1
				nodesInRow = nodesInRow + 1
				nodeIdxMat[i,j] = nodeIdx
		NIR[i] = nodesInRow
			
	# store fine mesh data into node data strucutres
	for i in range(yDim):
		for j in range(xDim):
			nodeIdx = nodeIdxMat[i,j]
			compIdx = fRadConfIdx[i,j]
			if (nodeIdx != -1):
				nodes.append(node())
				nodes[nodeIdx].compIdx = compIdx
				nodes[nodeIdx].hx      = hx[j]
				nodes[nodeIdx].hy      = hy[i]		
				nodes[nodeIdx].V       = hx[j]*hy[i]
				if (i == 0):
					nodes[nodeIdx].NNidx = -1
				else:
					nodes[nodeIdx].NNidx = nodeIdxMat[i-1,j]
				if (i == (yDim - 1)):
					nodes[nodeIdx].SNidx = -1
				else:
					nodes[nodeIdx].SNidx = nodeIdxMat[i+1,j]
				if (j == (xDim -1)):
					nodes[nodeIdx].ENidx = -1
				else:
					nodes[nodeIdx].ENidx = nodeIdxMat[i,j+1]
				if (j == 0):
					nodes[nodeIdx].WNidx = -1
				else:
					nodes[nodeIdx].WNidx = nodeIdxMat[i,j-1]
					
				# allocate the memory for node cross sections
				nodes[nodeIdx].Gx   = np.zeros(groups)
				nodes[nodeIdx].Gy   = np.zeros(groups)
				nodes[nodeIdx].Sa   = np.zeros(groups)
				nodes[nodeIdx].nSf  = np.zeros(groups)
				nodes[nodeIdx].kSf  = np.zeros(groups)				
				nodes[nodeIdx].Sgg  = np.zeros(groups)
				
				# set up adfs
				nodes[nodeIdx].adfN = np.ones(groups)
				nodes[nodeIdx].adfS = np.ones(groups)
				nodes[nodeIdx].adfE = np.ones(groups)
				nodes[nodeIdx].adfW = np.ones(groups)	
					
				if (adfN[i,j] == 1):
					for k in range(groups):
						nodes[nodeIdx].adfN[k] = comps[compIdx].adfN[k]
				if (adfS[i,j] == 1):
					for k in range(groups):
						nodes[nodeIdx].adfS[k] = comps[compIdx].adfS[k]
				if (adfE[i,j] == 1):
					for k in range(groups):
						nodes[nodeIdx].adfE[k] = comps[compIdx].adfE[k]
				if (adfW[i,j] == 1):
					for k in range(groups):
						nodes[nodeIdx].adfW[k] = comps[compIdx].adfW[k]																		

def getCompIdx(compName):
	global comps
	compIdx = -1
	i   = -1
	for comp in comps:
		i = i + 1
		if comps[i].name == compName:
			compIdx = i
			break
	return compIdx

def allocMem():
	
	global nnodes, phi1, phi2, powVec, A, Sfn, Sfnp1, Ss
	
	phi1   = np.zeros(nnodes)
	phi2   = np.zeros(nnodes)
	powVec = np.zeros(nnodes)
	A      = np.zeros([nnodes,nnodes])
	Sfn    = np.zeros(nnodes)
	Sfnp1  = np.zeros(nnodes)
	Ss     = np.zeros(nnodes)


def init():
	
	global nnodes, nodes, phi1, phi2, power

	updateNodeXS()
	
	# distribute initial flux evenly in regions with fissile material
	for i in range(nnodes):
		nSf2 = nodes[i].nSf[1]
		if (nSf2 > 0.0):
			phi1[i] = 1.0
			phi2[i] = 1.0
	pc = calcPower()
	mult =  power/pc
	phi1 = mult*phi1
	phi2 = mult*phi2

def calcPower():

	global nnodes, nodes, phi1, phi2, keff, powVec

	power = 0.0
	for  i in range(nnodes):
		kSf1 = nodes[i].kSf[0]
		kSf2 = nodes[i].kSf[1]
		V    = nodes[i].V
		powVec[i] = (kSf1*phi1[i]  + kSf2*phi2[i])*V
		power = power + powVec[i]
	return power

def updateNodeXS():
# this funcition is where XS updates will  take place
# since there is currently no TH feedback, it is called
# only once

	global nnodes, nodes, groups, comps 
	
	for i in range(nnodes):
		compIdx = nodes[i].compIdx
		hx      = nodes[i].hx
		hy      = nodes[i].hy
		for k in range(groups):
			D  = comps[compIdx].D[k]
			nodes[i].Gx[k]  = 2.0*D/hx
			nodes[i].Gy[k]  = 2.0*D/hy
			nodes[i].Sa[k]  = comps[compIdx].Sa[k]
			nodes[i].nSf[k] = comps[compIdx].nSf[k]
			nodes[i].kSf[k] = comps[compIdx].kSf[k]
			nodes[i].Sgg[k] = comps[compIdx].Sgg[k]

def calcFissionSource():
	
	global nnodes,  nnodes, phi1, phi2, Sfnp1
	
	for i in range(nnodes):
		nSf1     = nodes[i].nSf[0]
		nSf2     = nodes[i].nSf[1]
		V        = nodes[i].V
		Sfnp1[i] = (nSf1*phi1[i] + nSf2*phi2[i])*V

def calcScatSource():
	
	global nnodes, nodes, phi1, Ss
	
	for i  in range(nnodes):
		Ss12  = nodes[i].Sgg[0]
		V     = nodes[i].V
		Ss[i] = Ss12*phi1[i]*V 

def keffUpdate():
	
	global nnodes, Sfn, Sfnp1, keff
	
	ISfn   = 0.0
	ISfnp1 = 0.0
	
	for i in range(nnodes):
		ISfn   = ISfn   + Sfn[i]
		ISfnp1 = ISfnp1 + Sfnp1[i]
	
	keff = keff*ISfnp1/ISfn

def setupA(group):

	global A, nnodes, nodes, AlbedoN, AlbedoS, AlbedoE, AlbedoW
	
	for i in range(nnodes):
		# Get node data 
		hx    = nodes[i].hx					
		hy    = nodes[i].hy		
		V     = nodes[i].V
		GxP   = nodes[i].Gx[group]
		GyP   = nodes[i].Gy[group]
		Sa    = nodes[i].Sa[group]
		Sgg   = nodes[i].Sgg[group]
		adfPN = nodes[i].adfN[group]		
		adfPS = nodes[i].adfS[group]
		adfPE = nodes[i].adfE[group]		
		adfPW = nodes[i].adfW[group]	
		NNidx = nodes[i].NNidx
		SNidx = nodes[i].SNidx
		ENidx = nodes[i].ENidx
		WNidx = nodes[i].WNidx

		# Calculate Coefficients
		# North Face
		if (NNidx == -1):
			# Impose BC
			aPN = GyP*(1.0 - AlbedoN)*hx / (2.0*GyP*(1.0 + AlbedoN) + 1.0 - AlbedoN)
		else:
			# Get data for north neighbor
			GyN   = nodes[NNidx].Gy[group]
			adfNS = nodes[NNidx].adfS[group]  # adf on the south face of the north node
			# Calculate coefficients
			aTemp = GyP*GyN*hx /  (GyP*adfNS + GyN*adfPN)
			aPN   = aTemp*adfPN
			aN    = aTemp*adfNS
			# Place neighbor coefficient
			A[i,NNidx] = -aN
			
		# South Face
		if (SNidx == -1):
			# Impose BC
			aPS = GyP*(1.0 - AlbedoS)*hx / (2.0*GyP*(1.0 + AlbedoS) + 1.0 - AlbedoS)
		else:
			# Get data for north neighbor
			GyS   = nodes[SNidx].Gy[group]
			adfSN = nodes[SNidx].adfN[group]  # adf on the north face of the south node
			# Calculate coefficients
			aTemp = GyP*GyS*hx /  (GyP*adfSN + GyS*adfPS)
			aPS   = aTemp*adfPS
			aS    = aTemp*adfSN
			# Place neighbor coefficient
			A[i,SNidx] = -aS
			
		# East Face
		if (ENidx == -1):
			# Impose BC
			aPE = GxP*(1.0 - AlbedoE)*hy / (2.0*GxP*(1.0 + AlbedoE) + 1.0 - AlbedoE)
		else:
			# Get data for north neighbor
			GxE   = nodes[ENidx].Gx[group]
			adfEW = nodes[ENidx].adfW[group]  # adf on the west face of the east node
			# Calculate coefficients
			aTemp = GxP*GxE*hy /  (GxP*adfEW + GxE*adfPE)
			aPE   = aTemp*adfPE
			aE    = aTemp*adfEW
			# Place neighbor coefficient
			A[i,ENidx] = -aE
		
		# West Face
		if (WNidx == -1):
			# Impose BC
			aPW = GxP*(1.0 - AlbedoW)*hy / (2.0*GxP*(1.0 + AlbedoW) + 1.0 - AlbedoW)
		else:
			# Get data for north neighbor
			GxW   = nodes[WNidx].Gx[group]
			adfWE = nodes[WNidx].adfE[group]  # adf on the east face of the west node
			# Calculate coefficients
			aTemp = GxP*GxW*hy /  (GxP*adfWE + GxW*adfPW)
			aPW   = aTemp*adfPW
			aW    = aTemp*adfWE
			# Place neighbor coefficient
			A[i,WNidx] = -aW
			
		# Calculate center node coefficient
		aP = aPN + aPS + aPE + aPW + (Sa + Sgg)*V
		A[i,i] = aP								
			
def solveEig():
	
	global Sfnp1, Sfn, keff, phi1, Ss, phi2, eps, maxIt, A, power

	# Setup Coefficient Matrices
	setupA(0)
	A1 = np.copy(A)
	setupA(1)
	A2 = np.copy(A)

	calcFissionSource()
	iteration = 0
	print 'itr\tkeff\t\teps-Sf'
	while True:
		iteration = iteration + 1
		# Calculate updated fluxes
		Sfn = np.copy(Sfnp1)
		phi1 = np.linalg.solve(A1,Sfn/keff)
		calcScatSource()
		phi2 = np.linalg.solve(A2,Ss)
		# Calculate updated eigenvalue
		calcFissionSource()
		keffUpdate()
		# Normalize flux with power
		pc = calcPower()
		mult =  power/pc
		phi1 = mult*phi1
		phi2 = mult*phi2
		Sfnp1 = mult*Sfnp1		
		# Check convergence and take appropriate action
		SfR = np.linalg.norm(Sfnp1-Sfn,2)
		print '{0:6d}\t{1:.6f}\t{2:6e}'.format(iteration,keff,SfR)		
		if ((SfR < eps) or (iteration == maxIt)):
			break

def printNodes():
	global nnodes, NIR, nodes, yDim
	
	print 'compIdx'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].compIdx
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line
		
	print '\n'
	print 'hx'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].hx
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line	

	print '\n'
	print 'hy'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].hy
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line
		
	print '\n'
	print 'V'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].V
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line			
		
	print '\n'
	print 'NNidx'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].NNidx
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line
		
	print '\n'
	print 'SNidx'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].SNidx
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line				
		
	print '\n'
	print 'ENidx'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].ENidx
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line			
		
	print '\n'
	print 'WNidx'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].WNidx
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line			
		
	print '\n'
	print 'adfN - group 1'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].adfN[0]
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line
		
	print '\n'
	print 'adfN - group 2'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].adfN[1]
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line		
		
	print '\n'
	print 'adfS - group 1'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].adfS[0]
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line
		
	print '\n'
	print 'adfS - group 2'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].adfS[1]
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line			

	print '\n'
	print 'adfE - group 1'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].adfE[0]
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line
		
	print '\n'
	print 'adfE - group 2'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].adfE[1]
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line
		
	print '\n'
	print 'adfW - group 1'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].adfW[0]
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line
		
	print '\n'
	print 'adfW - group 2'
	nodeIdx = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			compIdx = nodes[nodeIdx].adfW[1]
			line = line + str(compIdx) + '\t'
			nodeIdx = nodeIdx + 1
		print line
		
def printVec(x):
	
	global yDim, NIR
	
	k = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			line = line + '{0:.6e}'.format(x[k]) + '\t'
			k = k + 1
		print line 

def printPhiADF(phi,face,group):
	
	global yDim, NIR, nodes
	
	k = 0
	for i in range(yDim):
		line = ''
		for j in range(NIR[i]):
			if (face=='N'):
				adf = nodes[k].adfN[group]
			elif (face=='S'):
				adf = nodes[k].adfS[group]
			elif (face=='E'):
				adf = nodes[k].adfE[group]
			elif (face=='W'):
				adf = nodes[k].adfW[group]
			z = adf*phi[k]											
			line = line + str(z) + '\t'
			k = k + 1
		print line 

def testInterface(group):
	
	global nnodes, nodes, NIR, phi1, phi2
	global AlbedoN, AlbedoS, AlbedoE, AlbedoW
	
	Jn = np.zeros(nnodes)
	Js = np.zeros(nnodes)
	Je = np.zeros(nnodes)
	Jw = np.zeros(nnodes)
	
	phin = np.zeros(nnodes)
	phis = np.zeros(nnodes)
	phie = np.zeros(nnodes)
	phiw = np.zeros(nnodes)
	

	groupStr = 'Group ' + str(group + 1)	
	if (group == 0):
		phi = np.copy(phi1)
	else:
		phi = np.copy(phi2)
	
	for i in range(nnodes):
		
		GxP   = nodes[i].Gx[group]
		GyP   = nodes[i].Gy[group]
		adfPN = nodes[i].adfN[group]		
		adfPS = nodes[i].adfS[group]
		adfPE = nodes[i].adfE[group]		
		adfPW = nodes[i].adfW[group]	
		NNidx = nodes[i].NNidx
		SNidx = nodes[i].SNidx
		ENidx = nodes[i].ENidx
		WNidx = nodes[i].WNidx
		
		# North Face
		if (NNidx == -1):
			# Impose BC
			phin[i] = GyP*phi[i] / (GyP + 0.5*(1.0 - AlbedoN)/(1.0 + AlbedoN))
		else:
			# Get data for north neighbor
			GyN   = nodes[NNidx].Gy[group]
			adfNS = nodes[NNidx].adfS[group]  # adf on the south face of the north node
			# Calculate coefficients
			den     = GyP + GyN*adfPN/adfNS
			phin[i] = (GyP*phi[i] + GyN*phi[NNidx])/den
		Jn[i] = GyP*(phi[i] - phin[i])
	
		# South Face
		if (SNidx == -1):
			# Impose BC
			phis[i] = GyP*phi[i] / (GyP + 0.5*(1.0 - AlbedoS)/(1.0 + AlbedoS))
		else:
			# Get data for north neighbor
			GyS   = nodes[SNidx].Gy[group]
			adfSN = nodes[SNidx].adfN[group]  # adf on the north face of the south node
			# Calculate coefficients
			den     = GyP + GyS*adfPS/adfSN
			phis[i] = (GyP*phi[i] + GyS*phi[SNidx])/den
		Js[i] = -GyP*(phi[i] - phis[i])

		# East Face
		if (ENidx == -1):
			# Impose BC
			phie[i] = GxP*phi[i] / (GxP + 0.5*(1.0 - AlbedoE)/(1.0 + AlbedoE))
		else:
			# Get data for north neighbor
			GxE   = nodes[ENidx].Gx[group]
			adfEW = nodes[ENidx].adfW[group]  # adf on the west face of the east node
			# Calculate coefficients
			den     = GxP + GxE*adfPE/adfEW
			phie[i] = (GxP*phi[i] + GxE*phi[ENidx])/den
		Je[i] = GxP*(phi[i] - phie[i])

		# East Face
		if (WNidx == -1):
			# Impose BC
			phiw[i] = GxP*phi[i] / (GxP + 0.5*(1.0 - AlbedoW)/(1.0 + AlbedoW))
		else:
			# Get data for north neighbor
			GxW   = nodes[WNidx].Gx[group]
			adfWE = nodes[WNidx].adfE[group]  # adf on the east face of the west node
			# Calculate coefficients
			den     = GxP + GxW*adfPW/adfWE
			phiw[i] = (GxP*phi[i] + GxW*phi[WNidx])/den
		Jw[i] = -GxP*(phi[i] - phiw[i])
	
	print '\n'	
	print groupStr + ' Jn'
	printVec(Jn)
	print '\n'
	print groupStr + ' Js'
	printVec(Js)
	print '\n'
	print groupStr + ' Je'
	printVec(Je)
	print '\n'
	print groupStr + ' Jw'
	printVec(Jw)
	print '\n'	
	print groupStr + ' phin fN'
	printPhiADF(phin,'N',group)
	print '\n'
	print groupStr + ' phis fS'
	printPhiADF(phis,'S',group)
	print '\n'
	print groupStr + ' phie fE'
	printPhiADF(phie,'E',group)
	print '\n'
	print groupStr + ' phiw fW'
	printPhiADF(phiw,'W',group)
	print '\n'					  				