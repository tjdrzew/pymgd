import pyMGDLib as MGD
import numpy as np

# Cross Section Data
MGD.comps.append(MGD.XS('A'))
MGD.comps[0].D    = np.array([1.3880   , 0.3842])
MGD.comps[0].Sa   = np.array([0.00971  , 0.1218])
MGD.comps[0].nSf  = np.array([0.006499 , 0.14290])
MGD.comps[0].kSf  = np.array([5.9494e-14 , 1.8314e-12])
MGD.comps[0].Sgg  = np.array([0.01422  , 0.0])
MGD.comps[0].adfW = np.array([0.9976   , 0.7996])
MGD.comps[0].adfE = np.array([1.0020   , 1.1390])
MGD.comps[0].adfS = np.array([0.9976   , 0.7996])
MGD.comps[0].adfN = np.array([1.0020   , 1.1390])

# Geometry
MGD.nxmesh    = 1
MGD.nymesh    = 1
MGD.xmesh     = np.array([8.0])
MGD.ymesh     = np.array([8.0])
MGD.nfxmesh   = np.array([1]) 
MGD.nfymesh   = np.array([16]) 

MGD.Rad_Conf = np.array([['A']])

# Boundary Conditions
MGD.AlbedoN = 1.0      # Reflective BC on North Face
MGD.AlbedoS = 0.0      # Vacuum BC on South Face
MGD.AlbedoE = 1.0      # Vacuum BC on East Face
MGD.AlbedoW = 1.0      # Reflective BC on West Face

# Solver Parameters
MGD.eps   = 1.0e-9
MGD.maxIt = 100
MGD.power = 1.0e-12

# Execute Code
MGD.pyMGD()
# print MGD.Sfn
# MGD.printNodes()
# MGD.testInterface(0)
# MGD.testInterface(1)



